import 'package:flutter/material.dart';

class Magazine{
  int id;
  String edition;
  String image_url;

  Magazine({
    @required this.id,
    @required this.edition,
    @required this.image_url
  });

}