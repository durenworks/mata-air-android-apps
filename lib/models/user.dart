import 'package:flutter/material.dart';

class User{
  String email;
  String name;
  String token;
  String password;
  Map otherInfo;
  bool loggedin;

  User({
    @required this.email,
    @required this.token,
    this.password,
    this.loggedin,
    this.name,
    this.otherInfo
  });
}