import 'package:flutter/material.dart';

class ThemeColor {
  
  static const bluedark = Color.fromRGBO(0, 83, 155, 1);

  static const eggblue = Color.fromRGBO(224, 239, 251, 1);

  static const darkgrey = Color.fromRGBO(74, 74, 74, 1);

}