import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/gestures.dart';

class ThirdTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ThirdTab();
  }
}

class _ThirdTab extends State<ThirdTab> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(top: 10.0),
              padding: EdgeInsets.all(20.0),
              child: RichText(
                text: TextSpan(children: [
                  TextSpan(
                    text: "Kami mempersembahkan One Stop Shopping for Digital ",
                    style: new TextStyle(color: Colors.black),
                  ),
                  TextSpan(
                    text: "Majalah Mata Air ",
                    style: new TextStyle(
                        color: Color.fromRGBO(0, 83, 155, 1),
                        fontWeight: FontWeight.bold),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        launch(
                            'https://docs.flutter.io/flutter/services/UrlLauncher-class.html');
                      },
                  ),
                  TextSpan(
                    text: "Pembaca akan menikmati koleksi lengkap ",
                    style: new TextStyle(color: Colors.black),
                  )
                ]),
              )),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            margin: EdgeInsets.only(bottom: 20.0),
            child: Text(
              "Paket Berlangganan Digital",
              style: TextStyle(
                  color: Color.fromRGBO(0, 83, 155, 1),
                  fontWeight: FontWeight.bold,
                  fontSize: 17.0),
            ),
          ),
          Card(
            color: Color.fromRGBO(224, 239, 251, 1),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Image.asset('assets/images/one_month.png'),
                Container(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "IDR. 35.0000",
                        style: TextStyle(
                          color: Colors.orange,
                          fontSize: 20.0,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10.0),
                        width: MediaQuery.of(context).size.width / 2,
                        child: Text(
                          "Akses 1 bulan Majalah Digital Mata Air",
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Card(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Image.asset('assets/images/one_year.png'),
                Container(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "IDR. 425.0000",
                        style: TextStyle(
                          color: Colors.orange,
                          fontSize: 20.0,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10.0),
                        width: MediaQuery.of(context).size.width / 2,
                        child: Text(
                          "Akses 1 tahun Majalah Digital Mata Air",
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
            child: Divider(
              height: 3,
              color: Colors.orange,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
            child: Text("Syarat & Ketentuan"),
          ),
        ],
      ),
    );
  }
}
