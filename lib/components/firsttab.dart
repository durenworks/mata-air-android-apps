import 'package:flutter/material.dart';
import '../models/magazine.dart';
import '../scoped-models/main.dart';

class FirstTab extends StatefulWidget {
  MainModel model;
  FirstTab(this.model);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FirstTab();
  }
}

class _FirstTab extends State<FirstTab> {
  List<Widget> gridContent = [];
  List<Map> dataGrid = [];
  int _currentIndex = 0;
  List<Magazine> magazines;

  bookGrid() {
    List<Magazine> magazines = widget.model.magazines;
    magazines.forEach((magazine) {
      print(magazine.image_url);
      print(magazine.edition);
      dataGrid.add({
        'image': magazine.image_url,
        'title': 'EDISI ' + magazine.edition,
        'type': 'ebook',
        'pages': '70',
      });
    });
    print('datagrid length');
    print(dataGrid.length);
    if (dataGrid.length > 0) {
      int skip = 0;
      int len = dataGrid.length;
      double widthimg = MediaQuery.of(context).size.width / 2 - 20;
      dataGrid.asMap().forEach((f, i) {
        print(skip);
        if ((skip == 0 || skip != f) && skip < len) {
          skip = f + 1;
          gridContent.add(Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    i['image'] != null
                        ? Image.network(
                            i['image'],
                            width: widthimg,
                          )
                        : Image.asset('assets/images/Book-Icon.png'),
                    Text(
                      i['title'],
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          // i['image'],
                          i['type'],
                          style: TextStyle(color: Colors.grey, fontSize: 13),
                        ),
                        Text(
                          ' ~  ' + i['pages'] + ' Halaman',
                          style: TextStyle(color: Colors.grey, fontSize: 13),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              skip < len
                  ? Container(
                      margin: EdgeInsets.only(bottom: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          dataGrid[skip]['image'] != null
                              ? Image.network(
                                  dataGrid[skip]['image'],
                                  width: widthimg,
                                )
                              : Image.asset('assets/images/Book-Icon.png'),
                          Text(
                            dataGrid[skip]['title'],
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Row(
                            children: <Widget>[
                              Text(
                                dataGrid[skip]['type'],
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 13),
                              ),
                              Text(
                                ' ~  ' + dataGrid[skip]['pages'] + ' Halaman',
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 13),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  : Container(),
            ],
          ));
        }
      });

      return gridContent;
    } else {
      return [
        Center(
          child: Text("data"),
        )
      ];
    }
  }

  @override
  void initState() {
    print("state first tab");
    if (widget.model.user == null) {
      widget.model.listNoLogin(10);
    } else {
      widget.model.listLogin(widget.model.user.token);
    }
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(horizontal: 0, vertical: 20.0),
          color: Color.fromRGBO(224, 239, 251, 1),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height / 3.5,
                width: MediaQuery.of(context).size.width / 2,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/Bitmap.png'),
                        fit: BoxFit.contain)),
              ),
              Container(
                height: MediaQuery.of(context).size.height / 3.5,
                width: MediaQuery.of(context).size.width / 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Terbaru',
                            style: TextStyle(
                              color: Color.fromRGBO(0, 83, 155, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 17,
                            ),
                          ),
                          Text(
                            'Edisi III 2018',
                            style: TextStyle(
                              color: Color.fromRGBO(0, 83, 155, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 17,
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Text(
                                'ebook',
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 13),
                              ),
                              Text(
                                ' ~ 123 Halaman',
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 13),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Berlangganan',
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 13),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5, bottom: 10),
                              child: Text(
                                'IDR 35.000 / Bulan',
                                style: TextStyle(
                                    color: Colors.orange,
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        )),
                    Container(
                      // height: 50.0,
                      padding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 25.0),
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey,
                                offset: Offset(1, 0),
                                blurRadius: 1.0)
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(25.0)),
                          gradient: LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                            colors: const <Color>[
                              Color.fromRGBO(0, 133, 197, 1),
                              Color.fromRGBO(0, 83, 155, 1),
                            ],
                          )),
                      child: Text(
                        "DAPATKAN",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: 15.0),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Arsip Majalar Mata Air",
                style: TextStyle(
                    color: Color.fromRGBO(0, 83, 155, 1),
                    fontWeight: FontWeight.bold),
              ),
              Container(
                child: Row(
                  children: <Widget>[
                    Text("Filter",
                        style: TextStyle(
                            color: Color.fromRGBO(0, 83, 155, 1),
                            fontWeight: FontWeight.bold)),
                    Icon(
                      Icons.keyboard_arrow_down,
                      color: Color.fromRGBO(0, 83, 155, 1),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        Container(
            padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
            child: widget.model.magazineLoading
                ? Center(
                    child: SizedBox(
                      height: 15.0,
                      width: 15.0,
                      child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                              Color.fromRGBO(0, 83, 155, 1))),
                    ),
                  )
                : Column(
                    children: bookGrid(),
                  ))
      ],
    );
  }
}
