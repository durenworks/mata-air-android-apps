import 'package:flutter/material.dart';

class SecondTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SecondTab();
  }
}

class _SecondTab extends State<SecondTab> {
  List<Widget> gridContent = [];
  List<Map> dataGrid = [];
  int _currentIndex = 0;

  bookGrid() {
    dataGrid.add({
      'image': 'assets/images/book_edisi_iv_2017.png',
      'title': 'EDISI IV 1 2007',
      'type': 'ebook',
      'pages': '70',
    });
    dataGrid.add({
      'image': 'assets/images/book_edisi_iv_2017.png',
      'title': 'EDISI IV 2 2007',
      'type': 'ebook',
      'pages': '70',
    });
    dataGrid.add({
      'image': null,
      'title': 'EDISI IV 333 2007',
      'type': 'ebook',
      'pages': '70',
    });
    
    if (dataGrid.length > 0) {
      int skip = 0;
      int len = dataGrid.length;
      dataGrid.asMap().forEach((f, i) {
        print(skip);
        if ((skip == 0 || skip != f) && skip < len) {
           skip = f + 1;
          gridContent.add(Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image.asset(
                      i['image'] == null
                          ? 'assets/images/Book-Icon.png'
                          : i['image'],
                    ),
                    Text(
                      i['title'],
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          i['type'],
                          style: TextStyle(color: Colors.grey, fontSize: 13),
                        ),
                        Text(
                          ' ~  ' + i['pages'] + ' Halaman',
                          style: TextStyle(color: Colors.grey, fontSize: 13),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              skip < (len -1) ?
              Container(
                margin: EdgeInsets.only(bottom: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image.asset(
                      dataGrid[skip]['image'] == null
                          ? 'assets/images/Book-Icon.png'
                          : dataGrid[skip]['image'],
                    ),
                    Text(
                      dataGrid[skip]['title'] ,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          dataGrid[skip]['type'],
                          style: TextStyle(color: Colors.grey, fontSize: 13),
                        ),
                        Text(
                          ' ~  ' + dataGrid[skip]['pages'] + ' Halaman',
                          style: TextStyle(color: Colors.grey, fontSize: 13),
                        ),
                      ],
                    ),
                  ],
                ),
              )
              : Container(),
            ],
          ));
        }
      });

      return gridContent;
    } else {
      return [
        Center(
          child: Text("data"),
        )
      ];
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Text("Filter",
                        style: TextStyle(
                            color: Color.fromRGBO(0, 83, 155, 1),
                            fontWeight: FontWeight.bold)),
                    Icon(
                      Icons.keyboard_arrow_down,
                      color: Color.fromRGBO(0, 83, 155, 1),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        Container(
            padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
            child: Column(
              children: bookGrid(),
            ))
      ],
    );
  }
}
