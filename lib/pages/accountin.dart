import 'package:flutter/material.dart';
import '../components/colors.dart';
import '../scoped-models/main.dart';

import '../models/user.dart';

class AccountIn extends StatefulWidget {
  MainModel model;
  AccountIn(this.model);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AccountIn();
  }
}

class _AccountIn extends State<AccountIn> {
  User user;
  @override
  void initState() {
    user = widget.model.user;
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: ThemeColor.eggblue,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(right: 25.0),
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () =>
                            Navigator.pushReplacementNamed(context, '/'),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: ThemeColor.bluedark,
                          size: 20.0,
                        ),
                      )
                    ],
                  )),
              Text(
                "Account",
                style: TextStyle(color: ThemeColor.bluedark),
              ),
            ],
          ),
        ),
        body: Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(Icons.account_circle,
                      color: ThemeColor.bluedark, size: 75.0),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                         widget.model.user.name,
                          style: TextStyle(fontSize: 15.0),
                        ),
                        Text(
                          widget.model.user.email,
                          style: TextStyle(color: Colors.grey, fontSize: 13.0),
                        )
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () =>
                        Navigator.pushReplacementNamed(context, '/accountedit'),
                    child: Icon(
                      Icons.edit,
                      color: ThemeColor.bluedark,
                    ),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 10.0),
                      child: Text(
                        "my purchases",
                        style: TextStyle(color: ThemeColor.bluedark),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 15.0),
                            child: Icon(Icons.notifications,
                                size: 20.0, color: ThemeColor.darkgrey),
                          ),
                          Expanded(
                            child: Text(
                              "Status Berlangganan",
                              style: TextStyle(color: ThemeColor.darkgrey),
                            ),
                          ),
                          GestureDetector(
                            child: Icon(
                              Icons.arrow_forward_ios,
                              size: 15.0,
                              color: ThemeColor.bluedark,
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 15.0),
                            child: Icon(Icons.library_books,
                                size: 20.0, color: ThemeColor.darkgrey),
                          ),
                          Expanded(
                            child: Text(
                              "Koleksi Majalah",
                              style: TextStyle(color: ThemeColor.darkgrey),
                            ),
                          ),
                          GestureDetector(
                            child: Icon(
                              Icons.arrow_forward_ios,
                              size: 15.0,
                              color: ThemeColor.bluedark,
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 10.0, top: 10.0),
                      child: Text(
                        "General",
                        style: TextStyle(color: ThemeColor.bluedark),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 15.0),
                            child: Icon(Icons.settings,
                                size: 20.0, color: ThemeColor.darkgrey),
                          ),
                          Expanded(
                            child: Text(
                              "Pengaturan",
                              style: TextStyle(color: ThemeColor.darkgrey),
                            ),
                          ),
                          GestureDetector(
                            child: Icon(
                              Icons.arrow_forward_ios,
                              size: 15.0,
                              color: ThemeColor.bluedark,
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 15.0),
                            child: Icon(Icons.info,
                                size: 20.0, color: ThemeColor.darkgrey),
                          ),
                          Expanded(
                            child: Text(
                              "Informasi Bantuan",
                              style: TextStyle(color: ThemeColor.darkgrey),
                            ),
                          ),
                          GestureDetector(
                            child: Icon(
                              Icons.arrow_forward_ios,
                              size: 15.0,
                              color: ThemeColor.bluedark,
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 15.0),
                            child: Icon(Icons.info,
                                size: 20.0, color: Colors.white),
                          ),
                          Expanded(
                            child: Text(
                              "privacy &policy",
                              style: TextStyle(color: ThemeColor.darkgrey),
                            ),
                          ),
                          GestureDetector(
                            child: Icon(
                              Icons.arrow_forward_ios,
                              size: 15.0,
                              color: ThemeColor.bluedark,
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 15.0),
                            child: Icon(Icons.info,
                                size: 20.0, color: Colors.white),
                          ),
                          Expanded(
                            child: Text(
                              "terms of conditions",
                              style: TextStyle(color: ThemeColor.darkgrey),
                            ),
                          ),
                          GestureDetector(
                            child: Icon(
                              Icons.arrow_forward_ios,
                              size: 15.0,
                              color: ThemeColor.bluedark,
                            ),
                          )
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        dynamic ress = widget.model.logout();
                        if(ress['result']){
                          Navigator.pushReplacementNamed(context, '/accountout');
                        }
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 50.0),
                        padding: EdgeInsets.symmetric(vertical: 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(right: 15.0),
                              child: Icon(Icons.input,
                                  size: 20.0, color: ThemeColor.bluedark),
                            ),
                            Expanded(
                              child: Text(
                                "Keluar",
                                style: TextStyle(color: ThemeColor.bluedark),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
