import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:flutter/gestures.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:email_validator/email_validator.dart';
import '../scoped-models/main.dart';

class AccountOut extends StatefulWidget {
  MainModel model;
  AccountOut(this.model);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AccountOut();
  }
}

class _AccountOut extends State<AccountOut>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  String name;
  String email;
  String password;
  String confirmedPass;
  bool passVisible;
  bool confirmedPassVisible;

  Map<String, dynamic> result;

  @override
  initState() {
    passVisible = false;
    confirmedPassVisible = false;
    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(_handleTabSelection);
    result = widget.model.result;
    super.initState();
  }

  void _handleTabSelection() {
    setState(() {});
  }

  alert(msg) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Information'),
          content: Text(msg),
          actions: <Widget>[
            FlatButton(
              child: Text('Okay'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  Widget masuk() {
    passVisible = passVisible == null ? false : passVisible;
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 20),
            child: TextField(
              keyboardType: TextInputType.emailAddress,
              onChanged: (String value) => email = value,
              decoration: InputDecoration(hintText: "Email"),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 40, bottom: 20.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width - 70,
                  child: TextField(
                    obscureText: passVisible,
                    onChanged: (String value) => password = value,
                    decoration: InputDecoration(hintText: "Katasandi"),
                  ),
                ),
                GestureDetector(
                  onTap: () => setState(() {
                        passVisible = !passVisible;
                      }),
                  child: passVisible
                      ? Icon(Icons.visibility_off)
                      : Icon(Icons.visibility),
                )
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text(
                "Lupa katasandi?",
                style: TextStyle(color: Color.fromRGBO(0, 83, 155, 1)),
              )
            ],
          ),
          GestureDetector(
           onTap: () async {
              print(email);
              if (password == null || email == null) {
                alert('Email atau password kosong');
              } else {
                if (email != null && EmailValidator.validate(email) == false) {
                  alert('Email tidak valid');
                } else {
                  result = await widget.model.login(email, password);
                  if (result['result']) {
                   Navigator.pushReplacementNamed(context, '/');
                  }
                }
              }
            },
            child: Container(
              margin: EdgeInsets.only(top: 40.0),
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey,
                        offset: Offset(1, 0),
                        blurRadius: 1.0)
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(25.0)),
                  gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    colors: const <Color>[
                      Color.fromRGBO(0, 133, 197, 1),
                      Color.fromRGBO(0, 83, 155, 1),
                    ],
                  )),
              child: widget.model.userLoading
                  ? Center(
                      child: SizedBox(
                        height: 15.0,
                        width: 15.0,
                        child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.white)),
                      ),
                    )
                  : Text(
                      "MASUK",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 15.0),
                    ),
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 40.0),
              padding: EdgeInsets.all(10.0),
              child: RichText(
                text: TextSpan(children: [
                  TextSpan(
                    text: "Belum punya akun? ",
                    style: new TextStyle(color: Colors.black),
                  ),
                  TextSpan(
                    text: "buat sekarang juga",
                    style: new TextStyle(
                      color: Colors.orange,
                    ),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        _tabController.animateTo((_tabController.index + 1));
                      },
                  ),
                ]),
              )),
        ],
      ),
    );
  }

  Widget daftar() {
    passVisible = passVisible == null ? false : passVisible;
    confirmedPassVisible =
        confirmedPassVisible == null ? false : confirmedPassVisible;
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 20),
            child: TextField(
              decoration: InputDecoration(hintText: "Name Lengkap"),
              onChanged: (String value) => name = value,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            child: TextField(
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(hintText: "Email"),
              onChanged: (String value) => email = value,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 40, bottom: 20.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width - 70,
                  child: TextField(
                    obscureText: passVisible,
                    decoration: InputDecoration(hintText: "Katasandi"),
                    onChanged: (String value) => password = value,
                  ),
                ),
                GestureDetector(
                  onTap: () => setState(() {
                        passVisible = !passVisible;
                      }),
                  child: passVisible
                      ? Icon(Icons.visibility_off)
                      : Icon(Icons.visibility),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 40, bottom: 20.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width - 70,
                  child: TextField(
                    obscureText: confirmedPassVisible,
                    decoration:
                        InputDecoration(hintText: "Konfirmasi katasandi"),
                    onChanged: (String value) => confirmedPass = value,
                  ),
                ),
                GestureDetector(
                  onTap: () => setState(() {
                        confirmedPassVisible = !confirmedPassVisible;
                      }),
                  child: confirmedPassVisible
                      ? Icon(Icons.visibility_off)
                      : Icon(Icons.visibility),
                )
              ],
            ),
          ),
          Container(
              child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(children: [
              TextSpan(
                text: "Membuat akun berarti anda menerima ",
                style: new TextStyle(color: Colors.black),
              ),
              TextSpan(
                text: "Syarat & ketentuan",
                style: new TextStyle(
                  color: Color.fromRGBO(0, 83, 155, 1),
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    _tabController.animateTo((_tabController.index + 1));
                  },
              ),
              TextSpan(
                text: " dan ",
                style: new TextStyle(color: Colors.black),
              ),
              TextSpan(
                text: "Kebijakan & Privasi",
                style: new TextStyle(
                  color: Color.fromRGBO(0, 83, 155, 1),
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    _tabController.animateTo((_tabController.index + 1));
                  },
              ),
              TextSpan(
                text: " Majalah Mata Air ",
                style: new TextStyle(color: Colors.black),
              ),
            ]),
          )),
          GestureDetector(
            onTap: () async {
              print(email);
              if (name == null || email == null) {
                alert('Nama atau email kosong');
              } else if (confirmedPass != password) {
                alert('Katasandi tidak cocok');
              } else {
                if (email != null && EmailValidator.validate(email) == false) {
                  alert('Email tidak valid');
                } else {
                  result = await widget.model.register(name, email, password);
                  if (result['result']) {
                    alert(result['message']);
                    Navigator.pushReplacementNamed(context, '/');
                  }
                }
              }
            },
            child: Container(
              margin: EdgeInsets.only(top: 40.0),
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey,
                        offset: Offset(1, 0),
                        blurRadius: 1.0)
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(25.0)),
                  gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    colors: const <Color>[
                      Color.fromRGBO(0, 133, 197, 1),
                      Color.fromRGBO(0, 83, 155, 1),
                    ],
                  )),
              child: widget.model.userLoading
                  ? Center(
                      child: SizedBox(
                        height: 15.0,
                        width: 15.0,
                        child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.white)),
                      ),
                    )
                  : Text(
                      "DAFTAR",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 15.0),
                    ),
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 40.0),
              padding: EdgeInsets.all(10.0),
              child: RichText(
                text: TextSpan(children: [
                  TextSpan(
                    text: "Sudah punya akun? ",
                    style: new TextStyle(color: Colors.black),
                  ),
                  TextSpan(
                    text: "Masuk disini",
                    style: new TextStyle(
                      color: Colors.orange,
                    ),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        _tabController.animateTo((_tabController.index - 1));
                      },
                  ),
                ]),
              )),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(
      Color.fromRGBO(224, 239, 251, 1),
      // Colors.white
    );

    // TODO: implement build
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
      return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                  width: MediaQuery.of(context).size.width / 3,
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () =>
                            Navigator.pushReplacementNamed(context, '/'),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: Color.fromRGBO(0, 83, 155, 1),
                          size: 20.0,
                        ),
                      )
                    ],
                  )),
              Text(
                "Account",
                style: TextStyle(color: Color.fromRGBO(0, 83, 155, 1)),
              ),
            ],
          ),
        ),
        body: Container(
          height: MediaQuery.of(context).size.height - 80,
          child: DefaultTabController(
            length: 2,
            child: Scaffold(
              appBar: AppBar(
                automaticallyImplyLeading: false,
                backgroundColor: Colors.grey[100],
                elevation: 0.1,
                flexibleSpace: SafeArea(
                    child: TabBar(
                  controller: _tabController,
                  indicatorColor: Colors.blue,
                  tabs: [
                    Tab(icon: Text("Masuk")),
                    Tab(icon: Text("Daftar")),
                  ],
                  labelColor: Color.fromRGBO(0, 83, 155, 1),
                  unselectedLabelColor: Colors.grey,
                )),
              ),
              body: TabBarView(
                controller: _tabController,
                children: [masuk(), daftar()],
              ),
            ),
          ),
        ),
      );
    });
  }
}
