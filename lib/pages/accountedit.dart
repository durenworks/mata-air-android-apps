import 'package:flutter/material.dart';
import '../components/colors.dart';
import 'package:flutter/gestures.dart';

class AccountEdit extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AccountEdit();
  }
}

class _AccountEdit extends State<AccountEdit>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  bool agree = false;
  void agreeChanged(bool value) => setState(() => agree = value);

  @override
  initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {});
  }

  Widget profilSection() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
      child: ListView(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(bottom: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "NAMA LENGKAP *",
                    style:
                        TextStyle(color: ThemeColor.bluedark, fontSize: 10.0),
                  ),
                  TextField(
                    style: TextStyle(fontSize: 13.0),
                    decoration: InputDecoration(hintText: "Nama lengkap"),
                  ),
                ],
              )),
          Container(
              margin: EdgeInsets.only(bottom: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "NOMOR HANDPHONE *",
                    style:
                        TextStyle(color: ThemeColor.bluedark, fontSize: 10.0),
                  ),
                  TextField(
                    style: TextStyle(fontSize: 13.0),
                    decoration:
                        InputDecoration(hintText: "Masukkan No Handphone"),
                  ),
                ],
              )),
          Container(
              margin: EdgeInsets.only(bottom: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Email *",
                    style:
                        TextStyle(color: ThemeColor.bluedark, fontSize: 10.0),
                  ),
                  TextField(
                    style: TextStyle(fontSize: 13.0),
                    decoration: InputDecoration(hintText: "Email"),
                  ),
                ],
              )),
          Container(
              margin: EdgeInsets.only(bottom: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "JUMLAH LANGGANAN *",
                    style:
                        TextStyle(color: ThemeColor.bluedark, fontSize: 10.0),
                  ),
                  TextField(
                    style: TextStyle(fontSize: 13.0),
                    decoration:
                        InputDecoration(hintText: "Masukkan No Handphone"),
                  ),
                ],
              )),
          Container(
              margin: EdgeInsets.only(bottom: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "ALAMAT PENGIRIMAN *",
                    style:
                        TextStyle(color: ThemeColor.bluedark, fontSize: 10.0),
                  ),
                  TextField(
                    style: TextStyle(fontSize: 13.0),
                    decoration:
                        InputDecoration(hintText: "Masukkan No Handphone"),
                  ),
                ],
              )),
          Container(
              margin: EdgeInsets.only(bottom: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "NAMA PERUMAHAN *",
                    style:
                        TextStyle(color: ThemeColor.bluedark, fontSize: 10.0),
                  ),
                  TextField(
                    style: TextStyle(fontSize: 13.0),
                    decoration:
                        InputDecoration(hintText: "Masukkan No Handphone"),
                  ),
                ],
              )),
          Container(
              margin: EdgeInsets.only(bottom: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "PROVINSI *",
                    style:
                        TextStyle(color: ThemeColor.bluedark, fontSize: 10.0),
                  ),
                  TextField(
                    style: TextStyle(fontSize: 13.0),
                    decoration:
                        InputDecoration(hintText: "Masukkan No Handphone"),
                  ),
                ],
              )),
          Container(
              margin: EdgeInsets.only(bottom: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "KOTA / KABUPATEN *",
                    style:
                        TextStyle(color: ThemeColor.bluedark, fontSize: 10.0),
                  ),
                  TextField(
                    style: TextStyle(fontSize: 13.0),
                    decoration:
                        InputDecoration(hintText: "Masukkan No Handphone"),
                  ),
                ],
              )),
          Container(
              margin: EdgeInsets.only(bottom: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "KECAMATAN *",
                    style:
                        TextStyle(color: ThemeColor.bluedark, fontSize: 10.0),
                  ),
                  TextField(
                    style: TextStyle(fontSize: 13.0),
                    decoration:
                        InputDecoration(hintText: "Masukkan No Handphone"),
                  ),
                ],
              )),
          Container(
              margin: EdgeInsets.only(bottom: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "KODE POS *",
                    style:
                        TextStyle(color: ThemeColor.bluedark, fontSize: 10.0),
                  ),
                  TextField(
                    style: TextStyle(fontSize: 13.0),
                    decoration:
                        InputDecoration(hintText: "Masukkan No Handphone"),
                  ),
                ],
              )),
          Container(
            child: Row(
              children: <Widget>[
                Container(
                  height: 20.0,
                  margin: EdgeInsets.only(right: 15.0),
                  width: 20.0,
                  decoration: BoxDecoration(
                    border: Border.all(color: ThemeColor.bluedark),
                  ),
                  child: Checkbox(
                      activeColor: ThemeColor.eggblue,
                      checkColor: ThemeColor.bluedark,
                      value: agree,
                      onChanged: agreeChanged),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      agree = !agree;
                    });
                  },
                  child: Text("Saya telah menerima syarat dan ketentuan"),
                )
              ],
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 40.0),
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey,
                        offset: Offset(1, 0),
                        blurRadius: 1.0)
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(25.0)),
                  gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    colors: const <Color>[
                      Color.fromRGBO(0, 133, 197, 1),
                      Color.fromRGBO(0, 83, 155, 1),
                    ],
                  )),
              child: Center(
                child: Text(
                  "SIMPAN",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 15.0),
                ),
              )),
        ],
      ),
    );
  }

  Widget transksiSection() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Card(
            color: Color.fromRGBO(224, 239, 251, 1),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Image.asset('assets/images/one_month.png'),
                Container(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "IDR. 35.0000",
                        style: TextStyle(
                          color: Colors.orange,
                          fontSize: 20.0,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10.0),
                        width: MediaQuery.of(context).size.width / 2 - 5,
                        child: Text(
                          "Akses 1 bulan Majalah Digital Mata Air",
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Card(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Image.asset('assets/images/one_year.png'),
                Container(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "IDR. 425.0000",
                        style: TextStyle(
                          color: Colors.orange,
                          fontSize: 20.0,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10.0),
                        width: MediaQuery.of(context).size.width / 2 - 5,
                        child: Text(
                          "Akses 1 tahun Majalah Digital Mata Air",
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: ThemeColor.eggblue,
        elevation: 0,
        bottom: TabBar(
          controller: _tabController,
          indicatorColor: ThemeColor.bluedark,
          tabs: [
            Tab(icon: Text("Profil")),
            Tab(icon: Text("Transaksi")),
          ],
          labelColor: ThemeColor.bluedark,
          unselectedLabelColor: Colors.grey,
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(right: 25.0),
                child: Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () => Navigator.pushReplacementNamed(context, '/'),
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: ThemeColor.bluedark,
                        size: 20.0,
                      ),
                    )
                  ],
                )),
            Text(
              "Detail Akun",
              style: TextStyle(color: ThemeColor.bluedark),
            ),
          ],
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height - 80,
        child: TabBarView(
          controller: _tabController,
          children: [
            profilSection(),
            transksiSection(),
          ],
        ),
      ),
    );
  }
}
