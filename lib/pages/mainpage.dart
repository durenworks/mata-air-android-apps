import 'package:flutter/material.dart';
import '../components/firsttab.dart';
import '../components/secondtab.dart';
import '../components/thirdtab.dart';
import 'package:url_launcher/url_launcher.dart';
import '../scoped-models/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/user.dart';

class Main extends StatefulWidget {
  MainModel model;

  Main(this.model);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Main();
  }
}

class _Main extends State<Main> with SingleTickerProviderStateMixin {
  TabController _tabController;
  int _currentIndex = 0;
  User user;

  void onTabTapped() {
    print(_tabController.index);
    if (_tabController.index == 3) {
      print("tapped 3");
      if (widget.model.user == null) {
        Navigator.pushReplacementNamed(context, '/accountout');
      } else {
        if (widget.model.user.loggedin) {
          Navigator.pushReplacementNamed(context, '/accountin');
        } else {
          Navigator.pushReplacementNamed(context, '/accountout');
        }
      }
    } else {
      setState(() {
        _currentIndex = _tabController.index;
      });
    }
  }

  initialize() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.get('token');
    widget.model.initialize();
  }

  @override
  initState() {
    initialize();
    super.initState();
    _tabController = new TabController(vsync: this, length: 4);
    _tabController.addListener(onTabTapped);
  }

  List<AppBar> appBarList = [
    AppBar(
      automaticallyImplyLeading: false,
      title: Container(
        padding: EdgeInsets.all(5.0),
        child: Image.asset('assets/images/blue_logo.png'),
      ),
      actions: <Widget>[
        GestureDetector(
          child: Icon(
            Icons.search,
            color: Color.fromRGBO(0, 83, 155, 1),
          ),
        )
      ],
      elevation: 0,
      backgroundColor: Color.fromRGBO(224, 239, 251, 1),
    ),
    AppBar(
      automaticallyImplyLeading: false,
      title: Text(
        "Koleksi Majalah",
        style: TextStyle(color: Color.fromRGBO(0, 83, 155, 1)),
      ),
      centerTitle: true,
      actions: <Widget>[
        GestureDetector(
          child: Icon(
            Icons.more_vert,
            color: Color.fromRGBO(0, 83, 155, 1),
          ),
        )
      ],
      elevation: 0,
      backgroundColor: Color.fromRGBO(224, 239, 251, 1),
    ),
    AppBar(
      automaticallyImplyLeading: false,
      title: Text("Berlangganan",
          style: TextStyle(color: Color.fromRGBO(0, 83, 155, 1))),
      centerTitle: true,
      actions: <Widget>[
        GestureDetector(
          child: Icon(
            Icons.share,
            color: Color.fromRGBO(0, 83, 155, 1),
          ),
        )
      ],
      elevation: 0,
      backgroundColor: Color.fromRGBO(224, 239, 251, 1),
    ),
    AppBar(
      automaticallyImplyLeading: false,
      title:
          Text("Akun", style: TextStyle(color: Color.fromRGBO(0, 83, 155, 1))),
      centerTitle: true,
      elevation: 0,
      backgroundColor: Color.fromRGBO(224, 239, 251, 1),
    )
  ];

  @override
  Widget build(BuildContext context) {
    print("model");
    print(widget.model.user);
    if (user != null) {
      print(user.token);
    }
    // TODO: implement build
    return DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: appBarList[_currentIndex],
          body: TabBarView(
            controller: _tabController,
            children: [
              FirstTab(widget.model),
              SecondTab(),
              ThirdTab(),
              null,
            ],
          ),
          bottomNavigationBar: new TabBar(
              controller: _tabController,
              tabs: [
                Tab(
                    icon: Container(
                  margin: EdgeInsets.only(top: 5.0),
                  child: Column(
                    children: <Widget>[
                      new Icon(Icons.local_library),
                      Container(
                        margin: EdgeInsets.only(top: 5.0),
                        child: Text(
                          "Baca Majalah",
                          style: TextStyle(fontSize: 8.0),
                        ),
                      )
                    ],
                  ),
                )),
                Tab(
                    icon: Container(
                  margin: EdgeInsets.only(top: 5.0),
                  child: Column(
                    children: <Widget>[
                      new Icon(Icons.library_books),
                      Container(
                          margin: EdgeInsets.only(top: 5.0),
                          child: Text(
                            "Koleksi Majalah",
                            style: TextStyle(fontSize: 8.0),
                          ))
                    ],
                  ),
                )),
                Tab(
                    icon: Container(
                  margin: EdgeInsets.only(top: 5.0),
                  child: Column(
                    children: <Widget>[
                      new Icon(Icons.notifications_active),
                      Container(
                          margin: EdgeInsets.only(top: 5.0),
                          child: Text(
                            "Berlangganan",
                            style: TextStyle(fontSize: 8.0),
                          ))
                    ],
                  ),
                )),
                Tab(
                    icon: Container(
                  margin: EdgeInsets.only(top: 5.0),
                  child: Column(
                    children: <Widget>[
                      new Icon(Icons.account_circle),
                      Container(
                          margin: EdgeInsets.only(top: 5.0),
                          child: Text(
                            "Akun",
                            style: TextStyle(fontSize: 8.0),
                          ))
                    ],
                  ),
                )),
              ],
              labelColor: Color.fromRGBO(0, 83, 155, 1),
              unselectedLabelColor: Colors.grey,
              indicator: UnderlineTabIndicator(
                  insets: EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0)),
              indicatorColor: Colors.white),
        ));
  }
}
