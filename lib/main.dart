import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'pages/mainpage.dart';
import 'pages/accountout.dart';
import 'pages/accountin.dart';
import 'pages/accountedit.dart';
import 'scoped-models/main.dart';

void main() => runApp(MataAir());

class MataAir extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MataAir();
  }
}

class _MataAir extends State<MataAir> {
  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(
      Color.fromRGBO(224, 239, 251, 1),
    );
    return ScopedModel<MainModel>(
        model: MainModel(),
        child: MaterialApp(
            theme: ThemeData(
              textTheme: Typography(platform: TargetPlatform.iOS).black,
            ),
            routes: {
              "/": (BuildContext context) => ScopedModelDescendant(builder:
                      (BuildContext context, Widget child, MainModel model) {
                    return Main(model);
                  }),
              "/accountout": (BuildContext context) =>
                  ScopedModelDescendant(builder:
                      (BuildContext context, Widget child, MainModel model) {
                    return AccountOut(model);
                  }),
              "/accountin": (BuildContext context) =>
                  ScopedModelDescendant(builder:
                      (BuildContext context, Widget child, MainModel model) {
                    return AccountIn(model);
                  }),
              "/accountedit": (BuildContext context) => AccountEdit(),
            }));
    // TODO: implement build
  }
}
