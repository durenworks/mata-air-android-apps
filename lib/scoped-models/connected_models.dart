import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:mime/mime.dart';
import 'package:http_parser/http_parser.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

class ConnectedModels extends Model {
  
  String _domain = "http://mata-air.lokavor.studio/";
  
  String get domain {
    return _domain;
  }

  writingFile(String text, String filename) async {
    print("writing " + filename);
    final directory = await getApplicationDocumentsDirectory();
    final file = File('${directory.path}/' + filename);
    await file.writeAsString(text);
  }

   Future<String> readFile(String filename) async {
    String text;
    try {
      final directory = await getApplicationDocumentsDirectory();
      final file = File('${directory.path}/' + filename);
      text = await file.readAsString();
    } catch (e) {
      print("Couldn't read file");
    }
    return text;
  }
   
}