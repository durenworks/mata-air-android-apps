import 'package:scoped_model/scoped_model.dart';

import './connected_models.dart';
import 'user.dart';
import 'magazine.dart';
// import './feeds.dart';

class MainModel extends Model with ConnectedModels, UserModels, MagazineModels { }
