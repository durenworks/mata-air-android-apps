import 'dart:convert';

import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';

import '../models/user.dart';
import 'connected_models.dart';

mixin UserModels on ConnectedModels {
  @override
  String get domain => super.domain;
  bool userLoading = false;
  User _authenticatedUser;
  Dio dio = new Dio();
  Map<String, dynamic> result = {'result': false};
  
  token() async {
     final SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.get('token');
      return token;
  }
  User get user {
    return _authenticatedUser;
  }

  readCredentialJson(String token) {
    Future credential = ConnectedModels().readFile("credential.json");
    credential.then((onValue) {
      // print(onValue['user']['name']);
      if (onValue != 'null') {
        Map<String, dynamic> json = jsonDecode(onValue);
        print(json['user']['id']);

        _authenticatedUser = User(
            name: json['user']['name'],
            email: json['user']['email'],
            token: token,
            loggedin: true,
            otherInfo: json['user']);
      } else {
        _authenticatedUser = null;
      }
      // Map json = jsonDecode(onValue)['user'];
      // print(json['name']);
    });

    return result = {'result': true, 'message': 'Registrasi berhasil'};
  }

  initialize() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.get('token');
    readCredentialJson(token);
  }

  getUser(String token) async {
    try {
      Response response = await dio.get(domain + 'api/user?token=' + token);
      dynamic responsedata = response.data;
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('token', token);
      prefs.setString('email', responsedata['user']['email']);
      ConnectedModels().writingFile(response.toString(), 'credential.json');
      return readCredentialJson(token);
    } catch (e) {
      return result = {
        'result': true,
        'message': 'Terjadi kesalahan saat mengunduh data user'
      };
    }
  }

  register(String name, String email, String password) async {
    userLoading = true;
    notifyListeners();
    try {
      Response response = await dio.post(domain + 'api/register',
          data: {'name': name, 'email': email, 'password': password},
          onSendProgress: (received, total) {});
      dynamic responsedata = response.data;

      print(responsedata['success']);
      print(responsedata['token']);
      result = await getUser(responsedata['token']);
    } catch (e) {
      result = {
        'result': true,
        'message': 'Terjadi kesalahan saat melakukan registrasi periksai email!'
      };
    }

    userLoading = false;
    notifyListeners();
    return result;
  }

  login(String email, String password) async {
    userLoading = true;
    notifyListeners();
    try {
      Response response = await dio.post(domain + 'api/login',
          data: {'email': email, 'password': password},
          onSendProgress: (received, total) {});
      dynamic responsedata = response.data;
      result = await getUser(responsedata['token']);
    } catch (e) {
      result = {
        'result': true,
        'message': 'Terjadi kesalahan saat melakukan registrasi periksai email!'
      };
    }

    userLoading = false;
    notifyListeners();
    return result;
  }

  logout() {
    ConnectedModels().writingFile('null', 'credential.json');
    return readCredentialJson('');
  }
}
