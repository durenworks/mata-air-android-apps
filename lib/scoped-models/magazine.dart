import 'dart:convert';

import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';

import '../models/user.dart';
import '../models/magazine.dart';
import 'connected_models.dart';

mixin MagazineModels on ConnectedModels {
  String get domain => super.domain;
  bool magazineLoading = false;
  List <Magazine> _magazines = [];

  Dio dio = new Dio();

  List<Magazine> get magazines {
    return List.from(_magazines);
  }

  listNoLogin(int limit) async {
    magazineLoading =true;
    _magazines = [];
    notifyListeners();
    Response response = await dio.get(domain + 'api/magazines?limit=');
    dynamic responsedata = response.data;

    responsedata.forEach((magazine) {
        Magazine mag = Magazine(
          id :magazine['id'],
          edition: magazine['edition'],
          image_url: magazine['image_url']
        );
        _magazines.add(mag);
    });

    print(responsedata);
    magazineLoading = false;
    notifyListeners();
    // print("grab magz list w/o login");
  }

   listLogin(String token) async {
    magazineLoading =true;
    _magazines = [];
    notifyListeners();
    Response response = await dio.get(domain + 'api/magazines?token=' + token);
    dynamic responsedata = response.data;

    responsedata.forEach((magazine) {
        Magazine mag = Magazine(
          id :magazine['id'],
          edition: magazine['edition'],
          image_url: magazine['image_url']
        );
        _magazines.add(mag);
    });

    print(responsedata);
    magazineLoading = false;
    notifyListeners();
    // print("grab magz list w/o login");
  }

}